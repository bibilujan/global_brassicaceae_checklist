---
title: "3.2-blt-check-tribes"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r}
clean_checklist <- read.csv(file = '/Users/beatrizlujantoro/global_brassicaceae_checklist_v2/data/interim/species_checklist_v2.5.4.csv', stringsAsFactors=FALSE)
clean_checklist <- read.csv(file = '/home/lujantorob/global_brassicaceae_checklist_v2/data/interim/species_checklist_v2.5.5.csv', stringsAsFactors=FALSE, row.names = 1)

```

Get a list of all tribes and the genera that belong to it, also check if there are any overlaps and issue warning if there is

```{r}
accepted <- subset(clean_checklist, ACC == "Y")
data_list <- list()
for (tribe in unique(accepted$TRIBE)) {
  data_list[[tribe]] <- unique(subset(accepted, TRIBE == tribe)$GENUS)
} 

all_genera <- length(unlist(data_list, use.names = FALSE))
all_genera2 <- length(unique(accepted$GENUS))

if (all_genera != all_genera2) {
  print("Warning, lengths are not the same")
}

```

Now, non accepted names, the tribe should be the synonym's genus' tribe

```{r}
non_accepted <- subset(clean_checklist, ACC == "N")
for (row in 1:nrow(non_accepted)) {
  tribe <- non_accepted$TRIBE[row]
  genus <- strsplit(non_accepted$SYNONYM_OF[row], " ")[[1]][1]
  #print(genus)
  if (genus %in% data_list[[tribe]]) {
    do <- "something"
  } else {
    print(paste(genus, "not in", tribe, sep = " "))
  }
  #data_list[[tribe]] <- unique(subset(accepted, TRIBE == tribe)$GENUS)
}

```
Andrzeiowskia is unassigned or unassigned #ressolved
Petrocallis is unassigned or Kernereae #res
Veselskya is unassigned or Anchonieae #res


Sisymbrieae

```{r}
clean_checklist <- read.csv(file = '/Users/beatrizlujantoro/global_brassicaceae_checklist_v2/data/interim/species_checklist_v2.5.5.csv', stringsAsFactors=FALSE)
```

```{r}
non_accepted <- subset(clean_checklist, ACC == "N")
for (row in 1:nrow(non_accepted)) {
  tribe <- non_accepted$TRIBE[row]
  genus <- strsplit(non_accepted$SYNONYM_OF[row], " ")[[1]][1]
  #print(genus)
  if (genus %in% data_list[[tribe]]) {
    do <- "something"
  } else {
    print(paste(genus, "not in", tribe, sep = " "))
  }
  #data_list[[tribe]] <- unique(subset(accepted, TRIBE == tribe)$GENUS)
}

```
