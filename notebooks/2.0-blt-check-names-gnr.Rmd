---
title: "Brassicaceae_checklist_workflow"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Workflow ## 
This notebook was started to cleanup Ardath's checklist, which is an update to the Global Brassicaceae checklist that was previously published in 2006. The following notebook outlines the workflow and the R scripts used to clean the data. 

## Install and load packages: ##

```{r}
options(repos = c(CRAN = "http://cran.rstudio.com"))
install.packages("taxize")
install.packages(c("dplyr", "magrittr"))
install.packages("RecordLinkage")
install.packages("readxl")
```
```{r}
library("taxize")
library("dplyr")
library("magrittr")
library(RecordLinkage)
library("readxl")

```

## 1. Read input excel  and write csv v2.0 ##
```{r}
raw_checklist <- read_excel(path ="/home/lujantorob/global_brassicaceae_checklist_v2/data/raw/Species Checklist V01AF2018.xlsx", sheet = "Modified Checklist")
names(raw_checklist)[1] <- "ACC"
names(raw_checklist)[2] <- "SYNONYM_OF"
head(raw_checklist)
write.csv(raw_checklist, file = "/home/lujantorob/global_brassicaceae_checklist_v2/data/interim/species_checklist_v2.0.csv", na= "")
```

## 2. read csv into open refine - export as csv v2.1 ##

The next step is done in open refine, by importing the checklist (v2.0), and working on each column to remove extra whitespace, facet text to search for mistakes in spelling, specially in the ACC, AY, type, tribe, genus and author. 

Then, the csv can be exported from open refine, into v2.1. 

*On the first iteration of this project, this wasn't done, but v2.csv was overwritten. I am also having difficulty exporting the history from open refine into a json file. (NO LONGER RELEVANT)

*I am having to redo the steps, so I will use v2.1


## 3. read csv - run taxize - output csv with scores ##

Taxize implements gnr, a tool to check scientific names against a number of databases and resources online. Here I read the csv and run gnr to resolve the names, using best match only, so if the name submitted matches to any database, then its returned as a match. 
The output is the same checklist with some extra columns, submitted name, matched name, score from gnr and the levenshteinSim score, which compares submitted and matched name to give a better score. 


```{r}
checkList <- read.csv(file = '/home/lujantorob/global_brassicaceae_checklist_v2/data/interim/species_checklist_v2.6.1.csv', stringsAsFactors=FALSE)
```

```{r}
for (row in 1:nrow(checkList)) {
  checkList$SUBMITTED_NAME[row] <- ""
  checkList$MATCHED_NAME[row] <- ""
  checkList$SCORE[row] <- ""
  checkList$SCORE2[row] <- ""
  checkList$DATABASE[row] <- ""
  if (checkList$SUBSP_VAR[row] == "") {
    acceptedName <- paste (checkList$GENUS[row], checkList$SPECIES[row] , sep = " ")
  } else {
    acceptedName <- paste (checkList$GENUS[row], checkList$SPECIES[row], checkList$SUBSP_VAR[row], sep = " ")
  }
  
  #print(acceptedName)
  out <- gnr_resolve(names = acceptedName, best_match_only = TRUE, canonical = TRUE, with_canonical_ranks = TRUE)
  if (is.data.frame(out) && nrow(out)==0) {
    checkList$SUBMITTED_NAME[row] <- acceptedName
    checkList$MATCHED_NAME[row] <- NA
    checkList$SCORE[row] <- 0
    checkList$SCORE2[row] <- 0
    checkList$DATABASE[row] <- NA
  } else {
  checkList$SUBMITTED_NAME[row] <- out$submitted_name
  checkList$MATCHED_NAME[row] <- out$matched_name2
  checkList$SCORE[row] <- out$score
  checkList$SCORE2[row] <- levenshteinSim(out$submitted_name, out$matched_name2)
  checkList$DATABASE[row] <- out$data_source_title
  }
}
write.csv(checkList, file = "/home/lujantorob/global_brassicaceae_checklist_v2/all_wscores.csv")
```

In the spreadsheet /data/interim/low_scores.xlsx, there are some notes on which names were evaluated and changed. When there was strong evidence that the suggested name was correct then it was changed:

Cardamine pseudotrifoliata ->	Cardamine "pseudotrifoliolata""
Cardamine astonieae	-> Cardamine "astoniae"
Phyllolepidium cyclocarpum ->	"Phyllolepidum"" cyclocarpum

1. Get the full names to search over it
```{r}
source("/home/lujantorob/global_brassicaceae_checklist_v2/src/r-functions/joinName.r")
checkList$FULL_NAME <- mapply(joinName, checkList$GENUS, checkList$SPECIES, checkList$SUBSP_VAR)
```

2. Change the names, got stuck on how to find the species only and not subsp./var. Also need to ask Ardath about Phyllolepidium (already in to_check spreadsheet)
```{r}
row <- grep("Cardamine pseudotrifoliata", checkList$FULL_NAME)
checkList$SPECIES[row] <- "pseudotrifoliolata"

row <- grep("Cardamine astonieae", checkList$FULL_NAME)
checkList$SPECIES[row] <- "astoniae"

#row <- grep('Phyllolepidium cyclocarpum$', checkList$FULL_NAME) ### how to grep exact only 
#checkList$GENUS[row] <- "Phyllolepidum"
#Other approach but also didnt work
#which(!is.na(str_match(checkList$FULL_NAME,'Phyllolepidium cyclocarpum')))
```

*Other potential issues were noted for Ardath to revise in the document /docs/to_check.xlsx


## 4. Check for duplicates ##

Used the following to create a list with the number of occurences of each submitted name. Then, went through the list and manually did the changes that were necessary. Ideally would have started with v2.1 and saved the new version as 2.2. However, I did the changes on the 2.csv directly.

--> better to run this function on accepted names only

```{r}
subAccepted <- subset(checkList, SYNONYM_OF == "")
n_occur <- data.frame(table(subAccepted$FULL_NAME))
```

Didn't find anything to delete, just one that was added to the to_check spreadsheet

## 5. Write new csv into processed directory ##

I have added a few things without testing, but the column names should be changed to remove spaces or periods, and the csv should be writen without the row names

```{r}
checkList$FULL_NAME <- NULL 
colnames(checkList)[colnames(checkList)=="DESIG.TYPE"] <- "DESIG_TYPE"
colnames(checkList)[colnames(checkList)=="DESIGN..BY"] <- "DESIGNATED_BY"
colnames(checkList)[colnames(checkList)=="DESIGNATION.SOURCE"] <- "DESIGNATION_SOURCE"
write.csv(checkList, file = "/home/lujantorob/global_brassicaceae_checklist_v2/data/processed/species_checklist_v2.3.csv", row.names = FALSE)
```


##### Note: ##### 

On the first iteration of the project, the file /home/lujantorob/global_brassicaceae_checklist_v2/data/processed/species_checklist_v2.csv contains the curated list that was fixed using open refine, taxize checks and dupe checks. 

I noticed that two columns had been split Type/Desig and the data was mixed between columns, not just shifted. I had to repeat the process, now I have data/processed/species_checklist_v2.3.csv as the processed checklist. It might be a good idea to rename the columns. 

Next step will be to add the changes that Ardath has made. This will be tracked in the next notebook. 

