# README #

This  repository contains the data and accompanying R and python notebooks used to produce the update of the global Brassicaceae checklist.
This checklist is an update from:

* Warwick, S.I., Francis, A. and Al-Shehbaz, I.A. (2006). Brassicaceae: Species checklist and database on CD-Rom. Pl. Syst. Evol. 259: 249─258.

The new checklist is available in GBIF, published through the pensoft IPT:

* Data paper: Ardath Francis, Beatriz E Lujan-Toro, Suzanne I Warwick, James A Macklin, Sara L Martin. 2021. Update on the Brassicaceae species checklist. Biodiversity Data Journal 9: e58773. doi: 10.3897/BDJ.9.e58773. https://bdj.pensoft.net/article/58773/list/8/
* GBIF: https://doi.org/10.15468/rb7kky
* IPT: http://ipt.pensoft.net/resource?r=aafc-brassicaceae-checklist

### What is this repository for? ###

The update on the checklist has been compiled by Ardath Francis on an Excel spreadsheet. Following this work, the data file was converted to .csv and was cleaned using open refine. The spreadsheet fields where then mapped to Darwin Core (DwC) terms to be published through an IPT and data paper with Pensoft.  

The names and authority in the checklist were also compared to IPNI and BrassiBase.

==============================
### Project Organization ###
------------

    ├── LICENSE
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The clean datasets converted to DwC and published in the IPT.
    │   └── raw            <- The original dabase and excel spreadsheets
    │
    ├── docs               <- Docs and notes relevant to the project
    │
    ├── notebooks          <- Jupyter and R notebooks. Naming convention is a number (for ordering),
    │                         the creators initials, and a short - delimited description, e.g.
    │                         1.0-jqp-initial-data-exploration.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    └── src                <- code to map spreadsheet to DwC terms and R functions
        └── r-functions  

### Who do I talk to? ###

* Beatriz Lujan Toro
* beatriz.lujan-toro@canada.ca

--------

Project organization based on the "https://drivendata.github.io/cookiecutter-data-science/" cookiecutter data science project template
