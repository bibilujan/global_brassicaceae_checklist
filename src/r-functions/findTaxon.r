findTaxon <- function(data_to_match, data_to_match_against)
  # Grep and find one match
  for (row in 1:nrow(data_to_match)) {
    matched_row_no <- grep(data_to_match$FULL_NAME[row], data_to_match_against$FULL_NAME)
    if (length(matched_row_no) == 1) {
      data_to_match$CHANGES[row] <- "matched"
      data_to_match_against[(matched_row_no),] <- data_to_match[row,] #change this based on the corrected list
      data_to_match_against$CHANGES[matched_row_no] <- "matched"
      
   # if two are found compare synonyms
    } else if (length(matched_row_no) > 1) {
      for (row2 in matched_row_no){
        name1 <- trimws(data_to_match$FULL_NAME[row])
        name2 <- trimws(data_to_match_against$FULL_NAME[row2])
        syn1 <- trimws(data_to_match$SYNONYM_OF[row])
        if (is.na(syn1)) {syn1 <- 0}
        syn2 <- trimws(data_to_match_against$SYNONYM_OF[row2])
        if (is.na(syn2)) {syn2 <- 0}
        if ((name1 == name2) && (syn1 == syn2)) {
          print(paste (name1, " = ",name2, syn1, " = ",syn2, sep = " "))
          data_to_match$CHANGES[row] <- "matched"
          data_to_match_against[(row2),] <- data_to_match[row,] #change this based on the corrected list
          data_to_match_against$CHANGES[row2] <- "matched"
        }
      }
    } else {
      print(paste (row, " NO MATCH"))
    }
  }