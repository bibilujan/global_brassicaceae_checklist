#Function to obtain the full name by joining the colums genus, species and subsp./var if applicable

joinName <- function(genus, species, subsp) {
  if (is.na(subsp)) {
    acceptedName <- paste (genus, species , sep = " ") 
  } else { 
    acceptedName <- paste (genus, species, subsp, sep = " ")
  }
  return(acceptedName)
}